$(document).ready(function() {
    $('.ui.checkbox').checkbox();

    $('input:text, .ui.button', '.ui.action.input')
        .on('click', function(e) {
            $('input:file', $(e.target).parents()).click();
        })
    ;

    $('input:file', '.ui.action.input')
        .on('change', function(e) {
            var name = e.target.files[0].name;
            $('input:text', $(e.target).parent()).val(name);
        })
    ;

    // $('.register').click(function() {
    //     var c = confirm('Make sure the information is valid. Ready to submit the form?');
    //     return c;
    // });

    $('.delete').click(function() {
        var c = confirm('Hapus, cog?');
        return c;
    });

    $('.edit').submit(function() {
        var c = confirm('Ganti, cog?');
        return c;
    });

    $('.paid').click(function() {
        var c = confirm('Sudah membayar, cog?');
        return c;
    });
});

// var response = '';
// var member1response = 'memberavailability1';
// var member2response = 'memberavailability2';
// var temp = document.getElementById('idtim').value;
// var member1val = document.getElementById('npmmember1').value;
// var member2val = document.getElementById('npmmember2').value;

// function showMemberAvailability(str, npmobj) {
//     var xmlhttp = new XMLHttpRequest();
//     xmlhttp.onreadystatechange = function() {
//         if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
//             document.getElementById(npmobj).innerHTML = xmlhttp.responseText;
//             response = xmlhttp.responseText;
//         }
//     };
//     xmlhttp.open('POST', 'ajaxnpm', true);
//     xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
//     xmlhttp.send('npm='+str);
// }
// showTeamAvailability(temp);
// showMemberAvailability(member1val, 'memberavailability1');
// showMemberAvailability(member2val, 'memberavailability2');

// function validateForm() {
//     member1val = document.getElementById('npmmember1').value;
//     member2val = document.getElementById('npmmember2').value;
//     if (response == 'Oops, team name has been taken :(') {
//         alert('Team name has been taken');
//         return false;
//     }
//     if (member1response == 'Sorry, your friend is registered :(' || member2response == 'Sorry, your friend is registered :(') {
//         alert('Team member has been registered!');
//         return false;
//     }
//     if (member1val == member2val) {
//         alert('Your team must consist of 3 members.');
//         return false;
//     }
// }

// $('form').submit(function() {
//     validateForm();
// });

// $('#idtim').keyup(function() {
//     showMemberAvailability($(this).value());
// });
