<?php
class Helper {
    private $db;
    private $settings;

    function __construct($db, $settings) {
        $this->db = $db;
        $this->settings = $settings;
    }

    public function isRegistered($npm) {
        $stmt = $this->db->prepare('SELECT * FROM users WHERE id = :npm');
        $stmt->execute(['npm' => $npm]);
        $user = $stmt->fetch();

        return $user;
    }

    public function isAdmin($npm) {
        $stmt = $this->db->prepare('SELECT * FROM admins WHERE id = :npm');
        $stmt->execute(['npm' => $npm]);
        $user = $stmt->fetch();

        return $user;
    }

    public function validateTeamNotExists($teamName) {
        $stmt = $this->db->prepare('SELECT * FROM teams WHERE name = :team_name');
        $stmt->execute(['team_name' => $teamName]);
        $team = $stmt->fetch();

        return $team;
    }

    public function validateNpmNotExist($id) {
        foreach ($id as $individualId) {
            $individualId = filter_var($individualId, FILTER_SANITIZE_STRING);
            $stmt = $this->db->prepare('SELECT * FROM users WHERE id = :id');
            $stmt->execute(['id' => $individualId]);
            $user = $stmt->fetch();

            if ($user) {
                return $individualId;
            }
        }

        return false;
    }

    public function redirect($response, $destination) {
        return $response->withStatus(302)->withHeader('Location',
            $this->settings['baseUrl'] . $destination);
    }
}
