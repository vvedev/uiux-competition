<?php

$config['debug'] = true;

$config['db']['host']   = 'localhost';
$config['db']['user']   = 'root';
$config['db']['pass']   = 'oasis';
$config['db']['dbname'] = 'uiux';

$config['caseStudy'] = 'https://drive.google.com/file/d/0BzpLCGkB-tVbWEVVSzYxekwzLWM/view?usp=sharing';

// Do not end with trailing slash
$config['baseUrl'] = 'http://localhost:8080';

// End of user configuration

$config['displayErrorDetails'] = $config['debug'];
