<?php
use Respect\Validation\Validator as v;

$container = $app->getContainer();

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

$container['session'] = function ($c) {
    return new \SlimSession\Helper;
};

$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig('templates', [
        'cache' => $c['settings']['debug'] ? false : 'cache',
        'debug' => $c['settings']['debug'],
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $c['router'],
        $c['request']->getUri()
    ));

    $view->getEnvironment()->addGlobal('flash', $c['flash']);
    $view->getEnvironment()->addGlobal('baseUrl', $c['settings']['baseUrl']);

    return $view;
};

$container['helper'] = function ($c) {
    return new \Helper($c['db'], $c['settings']);
};
