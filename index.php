<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \SSO\SSO;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use Mailgun\Mailgun;

require 'vendor/autoload.php';
require 'config.php';

date_default_timezone_set('Asia/Jakarta');
$app = new \Slim\App(['settings' => $config]);
$app->add(new \Slim\Middleware\Session([
    'autorefresh' => true,
    'lifetime' => '1 hour'
]));

require 'dependencies.php';

// ====================
// Public routes
// ====================
$app->view['foo'] = 'bar';
$app->get('/', function (Request $request, Response $response) {
    $session = $this->session;
    $npm = $session->user_id;

    if ($npm) {
        $user = $this->helper->isRegistered($npm);
    }

    return $this->view->render($response, 'index.html',
        compact('npm', 'user'));
});


$app->get('/about', function (Request $request, Response $response) {
    return $this->view->render($response, 'about.html');
});

$app->get('/login', function (Request $request, Response $response) {
    if (SSO::authenticate()) {
        $session = $this->session;
        $user = SSO::getUser();
        $npm = $user->npm;

        $session->user_id = $npm;
        $session->user_name = $user->name;

        if ($this->helper->isAdmin($npm)) {
            return $this->helper->redirect($response, '/admin');
        } else if ($this->helper->isRegistered($npm)) {
            return $this->helper->redirect($response, '/dashboard');
        } else {
            return $this->helper->redirect($response, '/registration');
        }
    }
});

$app->get('/logout', function (Request $request, Response $response) {
    $session = $this->session;

    if ($session->user_id) {
        $session::destroy();
        return $this->helper->redirect($response, '/logout');
    } else {
        SSO::logout();
    }
});


// ====================
// Participant routes
// ====================
$app->group('', function () use ($app) {
    $app->get('/registration', function (Request $request, Response $response) {
        $session = $this->session;
        $npm = $session->user_id;
        $nama = $session->user_name;

        if ($this->helper->isRegistered($npm)) {
            return $this->helper->redirect($response, '/dashboard');
        }

        return $this->view->render($response, 'registration.html',
            compact('npm', 'nama'));
    });

    $app->post('/registration', function (Request $request, Response $response) {
        $session = $this->session;
        $npm = $session->user_id;
        $nama = $session->user_name;

        if ($this->helper->isRegistered($npm)) {
            return $this->helper->redirect($response, '/dashboard');
        }

        $data = $request->getParsedBody();

        $teamName = filter_var($data['team_name'], FILTER_SANITIZE_STRING);
        $name = $data['name'];
        $id = $data['id'];
        $phone = $data['phone'];
        $email = $data['email'];
        $line = $data['line'];

        // Validators
        $rules = [];
        $rules['teamName'] = v::notEmpty()->setName('Team name');
        $rules['name'] = v::key(0, v::notEmpty()->alpha('.\''))->key(1, v::notEmpty()->alpha('.\''))->setName('Name');
        $rules['id'] = v::key(0, v::notEmpty()->numeric()->length(10, 10))->key(1, v::notEmpty()->numeric()->length(10, 10))->setName('Student number');
        $rules['phone'] = v::key(0, v::notEmpty()->numeric())->key(1, v::notEmpty()->numeric())->key(2, v::notEmpty()->numeric())->setName('Phone number');
        $rules['email'] = v::key(0, v::notEmpty()->email())->key(1, v::notEmpty()->email())->key(2, v::notEmpty()->email())->setName('Email');
        $rules['line'] = v::key(0, v::optional(v::alnum()))->key(1, v::optional(v::alnum()))->key(2, v::optional(v::alnum()))->setName('LINE ID');

        $messages = [
            'alnum' => 'LINE ID must only contain alpha numeric characters and dashes.',
            'alpha' => 'Name must only contain alphabetic characters.',
            'email' => 'Please make sure you typed a correct email address.',
            'length'  => 'Student No. must be 10 digits.',
            'notEmpty' => '{{name}} must not be empty.',
            'numeric' => '{{name}} must only contain numeric characters.',
        ];

        $errors = '';

        try {
            foreach ($rules as $rule => $validator) {
                $validator->assert($$rule);
            }

            if ($npm == $id[0] || $npm == $id[1] || $id[0] == $id[1]) {
                $errors = '# You have members with the same student member, a team must consist of 3 members.<br>';
            }

            if ($this->helper->validateTeamNotExists($teamName)) {
                $errors = $errors . "# Team name '$teamName' has already been registered.<br>";
            }

            $troubledNpm = $this->helper->validateNpmNotExist($id);
            if ($troubledNpm) {
                $errors = $errors . "# Student with number $troubledNpm has already been registered.<br>";
            }

            if (strlen($errors) > 0) {
                return $this->view->render($response, 'registration.html',
                    compact('npm', 'nama', 'data', 'errors'));
            } else {
                $stmt = $this->db->prepare('INSERT INTO teams(name, leader_id) VALUES (:name, :leader_id)');
                $stmt->execute(['name' => $teamName, 'leader_id' => $npm]);
                $teamId = $this->db->lastInsertId();

                $stmt = $this->db->prepare('INSERT INTO users(id, name, phone, email, line, team_id) VALUES (:id, :name, :phone, :email, :line, :team_id)');
                $stmt->execute([
                    'id' => $npm,
                    'name' => $session->user_name,
                    'phone' => filter_var($phone[0], FILTER_SANITIZE_NUMBER_INT),
                    'email' => filter_var($email[0], FILTER_SANITIZE_EMAIL),
                    'line' => filter_var($line[0], FILTER_SANITIZE_STRING),
                    'team_id' => $teamId
                ]);

                for ($i = 0; $i < 2; $i++) {
                    $stmt = $this->db->prepare('INSERT INTO users(id, name, phone, email, line, team_id) VALUES (:id, :name, :phone, :email, :line, :team_id)');
                    $stmt->execute([
                        'id' => filter_var($id[$i], FILTER_SANITIZE_NUMBER_INT),
                        'name' => filter_var($name[$i], FILTER_SANITIZE_STRING),
                        'phone' => filter_var($phone[$i + 1], FILTER_SANITIZE_NUMBER_INT),
                        'email' => filter_var($email[$i + 1], FILTER_SANITIZE_EMAIL),
                        'line' => filter_var($line[$i + 1], FILTER_SANITIZE_STRING),
                        'team_id' => $teamId
                    ]);
                }

                $client = new \Http\Adapter\Guzzle6\Client();
                $mg = new Mailgun('key-749a51f89188db72489859b4bbf1dba3', $client);

                $domain = 'sandbox1feefcc6f887408ebf64bb3d51ae6d5a.mailgun.org';

                $html = "<html>Team Name: $teamName<br>Leader's Name: $session->user_name<br>Leader's Email: $email[0]<br>Leader's Phone: $phone[0]<br>Leader's LINE ID: $line[0]<html>";

                $mg->sendMessage($domain, [
                    'from'    => 'Vvedev Ristek Fasilkom UI <ristek.ui@gmail.com>',
                    'to'      => 'ristek.ui@gmail.com',
                    'subject' => 'New UI/UX Competition Registrant',
                    'text'    => "$teamName, $session->user_name, $email[0], $phone[0], $line[0]",
                    'html'    => $html
                ]);

                $this->flash->addMessage('success', 'Your team has been succesfully registered.');
                return $this->helper->redirect($response, '/dashboard');
            }
        } catch(NestedValidationException $ex) {
            $validatorErrors = $ex->findMessages($messages);

            foreach ($validatorErrors as $validatorError) {
                if (!empty($validatorError)) {
                    $errors = $errors . $validatorError . '<br>';
                }
            }

            return $this->view->render($response, 'registration.html',
                compact('npm', 'nama', 'data', 'errors'));
        }
    });

    $app->get('/dashboard', function (Request $request, Response $response) {
        $session = $this->session;
        $npm = $session->user_id;

        if (!$this->helper->isRegistered($npm)) {
            return $this->helper->redirect($response, '/registration');
        }

        $nama = $session->user_name;

        $stmt = $this->db->prepare('SELECT team_id FROM users WHERE id = :id');
        $stmt->execute(['id' => $npm]);
        $teamId = $stmt->fetch()['team_id'];

        $stmt = $this->db->prepare('SELECT * FROM users WHERE team_id = :team_id');
        $stmt->execute(['team_id' => $teamId]);
        $users = $stmt->fetchAll();

        $stmt = $this->db->prepare('SELECT submission FROM teams WHERE id = :team_id');
        $stmt->execute(['team_id' => $teamId]);
        $submission = $stmt->fetch()['submission'];

        $caseStudy = $this->settings['caseStudy'];

        return $this->view->render($response, 'dashboard.html',
            compact('nama', 'users', 'submission', 'caseStudy'));
    });

    $app->post('/dashboard', function (Request $request, Response $response) {
        $session = $this->session;
        $npm = $session->user_id;

        if (!$this->helper->isRegistered($npm)) {
            return $this->helper->redirect($response, '/registration');
        }

        $stmt = $this->db->prepare('SELECT team_id FROM users WHERE id = :id');
        $stmt->execute(['id' => $npm]);
        $teamId = $stmt->fetch()['team_id'];

        $data = $request->getParsedBody();
        $submission = filter_var($data['submission'], FILTER_SANITIZE_STRING);
        $stmt = $this->db->prepare('UPDATE teams SET submission = :submission WHERE id = :team_id');
        $stmt->execute(['submission' => $submission, 'team_id' => $teamId]);

        $this->flash->addMessage('success', 'Thanks! We have received your submission.');
        return $this->helper->redirect($response, '/dashboard');
    });
})->add(function ($request, $response, $next) {
    $session = $this->session;
    if (!$this->session->user_id) {
        return $this->helper->redirect($response, '/login');
    }

    $response = $next($request, $response);
    return $response;
});


// ====================
// Admin routes
// ====================
$app->group('', function () use ($app) {
    $app->get('/admin', function (Request $request, Response $response) {
        $stmt = $this->db->prepare('SELECT *, users.name as user_name, teams.name as team_name, users.id as user_id FROM users, teams WHERE users.team_id = teams.id');
        $stmt->execute();
        $users = $stmt->fetchAll();

        return $this->view->render($response, 'admin.html',
            compact('users'));
    });

    $app->get('/admin/{user_id}/edit', function (Request $request, Response $response, $args) {
        $userId = $args['user_id'];
        $stmt = $this->db->prepare('SELECT *, users.name as user_name, teams.name as team_name FROM users, teams WHERE users.id = :user_id AND users.team_id = teams.id');
        $stmt->execute(['user_id' => $userId]);
        $user = $stmt->fetch();

        return $this->view->render($response, 'edit.html',
            compact('user'));
    });

    $app->post('/admin/{user_id}/edit', function (Request $request, Response $response, $args) {
        $userId = $args['user_id'];
        $data = $request->getParsedBody();
        $userName = $data['name'];

        $stmt = $this->db->prepare('UPDATE users SET name = :name, phone = :phone, email = :email, line = :line WHERE id = :user_id');
        $stmt->execute([
            'name' => $userName,
            'phone' => $data['phone'],
            'email' => $data['email'],
            'line' => $data['line'],
            'user_id' => $userId
        ]);

        $stmt = $this->db->prepare('SELECT team_id FROM users WHERE id = :user_id');
        $stmt->execute(['user_id' => $userId]);
        $teamId = $stmt->fetch()['team_id'];

        $stmt = $this->db->prepare('UPDATE teams SET name = :team_name WHERE id = :team_id');
        $stmt->execute([
            'team_name' => $data['team_name'],
            'team_id' => $teamId
        ]);

        $this->flash->addMessage('success', "$userName is edited, cog.");
        return $this->helper->redirect($response, '/admin');
    });

    $app->get('/admin/{team_id}/delete', function (Request $request, Response $response, $args) {
        $teamId = $args['team_id'];
        $stmt = $this->db->prepare('DELETE FROM teams WHERE id = :team_id');
        $stmt->execute(['team_id' => $teamId]);

        $this->flash->addMessage('success', 'Team is deleted, cog.');
        return $this->helper->redirect($response, '/admin');
    });

    $app->get('/admin/{team_id}/paid', function (Request $request, Response $response, $args) {
        $teamId = $args['team_id'];
        $stmt = $this->db->prepare('UPDATE teams SET has_paid = 1 WHERE id = :team_id');
        $stmt->execute(['team_id' => $teamId]);

        $this->flash->addMessage('success', "Team $teamName has paid, cog.");
        return $this->helper->redirect($response, '/admin');
    });
})->add(function ($request, $response, $next) {
    $session = $this->session;
    if (!$this->helper->isAdmin($this->session->user_id)) {
        return $this->helper->redirect($response, '/');
    }

    $response = $next($request, $response);
    return $response;
});

$app->run();
