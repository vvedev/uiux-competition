# UI/UX Competition Website

Dear Akbar,

If you're reading this, we're sorry that we melangkahi your code. We are making this just in case you are too busy with CompFest ada di hati.

We still love you,
and perhaps your code (sometimes, when it works).

Regards,  
Senior members of Vvedev  
With the Head of SIG's approval

## Installation Instructions
1. Run `composer install`
2. Import the database schema and configure your database connection in `config.php`
3. Start your PHP server i.e. `php -S localhost:8080`
4. Open `http://localhost:8080/` in your favorite browser. Voilà!
